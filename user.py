from .gorest_requests_class import GorestRequests


class UserClass:

    def __init__(self, base_url: str = "https://gorest.co.in/public-api/users"):
        self.base_url = base_url
        self.gorest = GorestRequests(acces_token="Bearer gSXtNUE7mO3yf9BehXygw4hMXS2aRoihe4E1")

    def changing_name(self, user_id, changed_name: str = None):
        dictionary = {'first_name': changed_name}
        self.gorest.put_request(
           url=f"{self.base_url}/{user_id}",
           body=dictionary)

    def display_info(self, user_id: str = None):
        if user_id is not None:
            result = self.gorest.get_request(
                url=f"{self.base_url}/{user_id}"
            )
        else:
            result = self.gorest.get_request(
                url=self.base_url)
        return result

    def register_user(self, register_data : dict = None):
        result = self.gorest.post_request(
           url=self.base_url,
           body=register_data)
        return result
