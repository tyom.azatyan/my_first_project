from ..user import UserClass


def test_user_edit():
    user_test = UserClass()

    desired_name = "Artash"
    desired_user = {
        'first_name': desired_name,
        'last_name': "Carukyan",
        'gender': "male",
        'email': "CarukeyalmnRulitVPolnoch@mail.ru"}

    # Step1: Create desired user
    created_user = user_test.register_user(register_data=desired_user)
    created_user_id = created_user['id']

    # Step2: Get newly created user
    created_user_get = user_test.display_info(user_id=created_user_id)
    created_user_first_name = created_user_get['first_name']

    # Step3: Assert that user is correctly created
    assert created_user_first_name == desired_name

    # Step4: Changed the user
    changed_name = "Tufta"
    user_test.changing_name(user_id=created_user_id, changed_name=changed_name)

    # Step5: Get changed user
    created_user_changed_get = user_test.display_info(user_id=created_user_id)
    created_user_changed_name = created_user_changed_get['first_name']

    # Step6: Assert that user is correctly changed
    assert created_user_changed_name == changed_name







