import requests
import json


class GorestRequests:

    def __init__(self, acces_token: str = "Bearer gSXtNUE7mO3yf9BehXygw4hMXS2aRoihe4E1"):
        self.access_token = acces_token

    def get_request(self, url):
        response = requests.get(
            url=url,
            headers={"Authorization": self.access_token})
        result = json.loads(response.text)['result']
        return result

    def put_request(self, url: str, body: dict = {}):
        response = requests.put(
            url=url,
            headers={"Authorization": self.access_token},
            data=body
        )
        result = json.loads(response.text)['result']
        return result

    def post_request(self, url: str, body: dict = {}):
        response = requests.post(
            url=url,
            headers={"Authorization": self.access_token},
            data=body
        )
        result = json.loads(response.text)['result']
        return result



